package br.com.unifg.api.bikelane.dto;

import java.io.Serializable;

import br.com.unifg.api.bikelane.domain.BikeLane;

public class BikeLaneDTO implements Serializable {
	
	private static final long serialVersionUID = 7646768873713751501L;

	private String _id;
	private String rota;
	private String sentido;
	private String percurso;
	private String bairro;
	private String extensao_km;
	private String inauguracao;
	private String km_acumulada;

	public BikeLaneDTO() {
		super();
	}

	public BikeLaneDTO(BikeLane bikeLane) {

		this.set_id(bikeLane.getId());
		this.setRota(bikeLane.getRoute());
		this.setSentido(bikeLane.getWay());
		this.setPercurso(bikeLane.getCourse());
		this.setBairro(bikeLane.getDistrict());
		this.setExtensao_km(bikeLane.getExtent());
		this.setInauguracao(bikeLane.getInauguration());
		this.setKm_acumulada(bikeLane.getAccumulated());
	}

	public BikeLane toDocument() {

		BikeLane document = new BikeLane();

		document.setId(this.get_id());
		document.setRoute(this.getRota());
		document.setWay(this.getSentido());
		document.setCourse(this.getPercurso());
		document.setDistrict(this.getBairro());
		document.setExtent(this.getExtensao_km());
		document.setInauguration(this.getInauguracao());
		document.setAccumulated(this.getKm_acumulada());

		return document;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String id) {
		this._id = id;
	}

	public String getRota() {
		return rota;
	}

	public void setRota(String rota) {
		this.rota = rota;
	}

	public String getSentido() {
		return sentido;
	}

	public void setSentido(String sentido) {
		this.sentido = sentido;
	}

	public String getPercurso() {
		return percurso;
	}

	public void setPercurso(String percurso) {
		this.percurso = percurso;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getExtensao_km() {
		return extensao_km;
	}

	public void setExtensao_km(String extensao_km) {
		this.extensao_km = extensao_km;
	}

	public String getInauguracao() {
		return inauguracao;
	}

	public void setInauguracao(String inauguracao) {
		this.inauguracao = inauguracao;
	}

	public String getKm_acumulada() {
		return km_acumulada;
	}

	public void setKm_acumulada(String km_acumulada) {
		this.km_acumulada = km_acumulada;
	}

}
