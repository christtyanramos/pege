package br.com.unifg.api.profile.dto;

import java.io.Serializable;
import java.util.List;

import br.com.unifg.api.bikelane.domain.BikeLane;
import br.com.unifg.api.profile.domain.Profile;

public class ProfileDTO implements Serializable {
	
	private static final long serialVersionUID = 731529069907828434L;

	private String id;
	private List<BikeLane> favorites;
	private List<BikeLane> recentSearches;

	public ProfileDTO() {
		super();
	}
	
	public ProfileDTO(Profile profile) {
		
		this.setId(profile.getId());
		
		if (profile.getFavorites() != null && !profile.getFavorites().isEmpty()) {
			this.setFavorites(profile.getFavorites());			
		}
		
		if (profile.getRecentSearches() != null && !profile.getRecentSearches().isEmpty()) {
			this.setRecentSearches(profile.getRecentSearches());			
		}
		
	}
	
	public Profile toDocument() {
	
		Profile document = new Profile();
		
		document.setId(this.getId());
		
		if (this.getFavorites() != null && !this.getFavorites().isEmpty()) {
			document.setFavorites(this.getFavorites());			
		}
		
		if (this.getRecentSearches() != null && !this.getRecentSearches().isEmpty()) {
			document.setRecentSearches(this.getRecentSearches());			
		}
		
		return document;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<BikeLane> getFavorites() {
		return favorites;
	}

	public void setFavorites(List<BikeLane> favorites) {
		this.favorites = favorites;
	}

	public List<BikeLane> getRecentSearches() {
		return recentSearches;
	}

	public void setRecentSearches(List<BikeLane> recentSearches) {
		this.recentSearches = recentSearches;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfileDTO other = (ProfileDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
