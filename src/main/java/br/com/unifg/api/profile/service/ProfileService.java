package br.com.unifg.api.profile.service;

import br.com.unifg.api.profile.domain.Profile;

public interface ProfileService {

	public Profile findById(String id);

	public Profile saveOrUpdate(Profile profile);

	public void deleteFavorite(String profileId, String bikeLaneId);

}