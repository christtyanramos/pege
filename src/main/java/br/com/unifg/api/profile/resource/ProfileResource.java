package br.com.unifg.api.profile.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unifg.api.bikelane.domain.BikeLane;
import br.com.unifg.api.bikelane.dto.BikeLaneDTO;
import br.com.unifg.api.exception.ObjectAlreadyExistingException;
import br.com.unifg.api.profile.domain.Profile;
import br.com.unifg.api.profile.service.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api/profiles")
@Api(value = "API REST Perfil")
@CrossOrigin(origins = "*")
public class ProfileResource {

	@Autowired
	private ProfileService profileService;

	@GetMapping("/{profileId}/favorites")
	@ApiOperation(tags = { "Perfis" }, value = "Lista as ciclofaixas favoritas do perfil.")
	public ResponseEntity<List<BikeLaneDTO>> listFavorites(@PathVariable String profileId) {

		Profile profile = this.profileService.findById(profileId);

		List<BikeLane> favorites = new ArrayList<>(profile.getFavorites());

		List<BikeLaneDTO> resources = new ArrayList<>();
		for (BikeLane favorite : favorites) {
			resources.add(new BikeLaneDTO(favorite));
		}

		return new ResponseEntity<>(resources, HttpStatus.OK);
	}

	@PostMapping("/{profileId}/favorites")
	@ApiOperation(tags = { "Perfis" }, value = "Favorita uma ciclofaixa.")
	public ResponseEntity<BikeLaneDTO> favorite(@PathVariable String profileId,
			@Valid @RequestBody BikeLaneDTO resource) {

		Profile profile = this.profileService.findById(profileId);
		
		List<BikeLane> bikelanes = profile.getFavorites()
										  .stream()
										  .filter(bikelane -> bikelane.getRoute().equals(resource.getRota()))
										  .collect(Collectors.toList());
		
		if(bikelanes.isEmpty()) {			
			profile.getFavorites().add(resource.toDocument());
			profile = this.profileService.saveOrUpdate(profile);
		} else {
			throw new ObjectAlreadyExistingException("Objeto já se encontra na lista!");
		}
		

		return new ResponseEntity<>(new BikeLaneDTO(resource.toDocument()), HttpStatus.CREATED);
	}

	@DeleteMapping("/{profileId}/favorites/{bikeLaneId}")
	@ApiOperation(tags = { "Perfis" }, value = "Desfavorita uma ciclofaixa.")
	public ResponseEntity<Void> delete(@PathVariable String profileId, @PathVariable String bikeLaneId) {

		this.profileService.deleteFavorite(profileId, bikeLaneId);

		return new ResponseEntity<>(HttpStatus.OK);
	}

}
