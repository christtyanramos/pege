package br.com.unifg.api.profile.service;

import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unifg.api.exception.ObjectNotFoundException;
import br.com.unifg.api.profile.domain.Profile;
import br.com.unifg.api.profile.repository.ProfileRepository;

@Service
public class ProfileServiceImpl implements ProfileService {
	
	@Autowired
	private ProfileRepository profileRepository;

	@Override
	public Profile findById(String id) {
		
		Optional<Profile> document = this.profileRepository.findById(id);

		return document.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! id: " + id + ", Tipo: " + Profile.class.getName()));
	}

	@Override
	public Profile saveOrUpdate(Profile profile) {
		return this.profileRepository.save(profile);
	}

	@Override
	public void deleteFavorite(String profileId, String bikeLaneId) {
		
		Profile document = this.profileRepository.findById(profileId).get();
		
		document.setFavorites(document.getFavorites()
									  .stream()
									  .filter(bikeLane -> !bikeLane.getId().equals(bikeLaneId))
									  .collect(Collectors.toList()));
		
		this.profileRepository.save(document);
	}

}
