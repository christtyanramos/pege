package br.com.unifg.api.user.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import br.com.unifg.api.profile.dto.ProfileDTO;
import br.com.unifg.api.user.domain.User;

/**
 * DTO que representa o usuário.
 */
public class UserDTO implements Serializable {

	private static final long serialVersionUID = 2888034955495293783L;

	private String id;

	@NotNull(message = "Preenchimento obrigatório.")
	private String name;

	@NotNull(message = "Preenchimento obrigatório.")
	@Email(message = "Email inválido")
	private String email;

	private String password;

	private ProfileDTO profile;

	public UserDTO() {
		super();
	}

	public UserDTO(User user) {
		this.setId(user.getId());
		this.setName(user.getName());
		this.setEmail(user.getEmail());
		this.setProfile(user.getProfile() != null ? new ProfileDTO(user.getProfile()) : null);
	}

	public User toDocument() {

		User document = new User();

		document.setId(this.getId());
		document.setName(this.getName());
		document.setEmail(this.getEmail().toLowerCase());
		document.setPassword(this.getPassword());
		document.setProfile(this.getProfile() != null ? this.getProfile().toDocument() : null);

		return document;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ProfileDTO getProfile() {
		return profile;
	}

	public void setProfile(ProfileDTO profile) {
		this.profile = profile;
	}

}
