package br.com.unifg.api.user.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.unifg.api.exception.WrongIdException;
import br.com.unifg.api.user.domain.User;
import br.com.unifg.api.user.dto.UserDTO;
import br.com.unifg.api.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api/users")
@Api(value = "API REST Usuario")
@CrossOrigin(origins = "*")
public class UserResource {

	@Autowired
	private UserService userService;

	@GetMapping("/{id}")
	@ApiOperation(tags = { "Usuarios" }, value = "Consulta um usuário por id.")
	public ResponseEntity<?> get(@PathVariable String id) {

		User document = this.userService.findById(id);

		return new ResponseEntity<>(new UserDTO(document), HttpStatus.OK);
	}

	@GetMapping("/email")
	@ApiOperation(tags = { "Usuarios" }, value = "Consulta um usuário por email.")
	public ResponseEntity<?> findByEmail(@RequestParam(value = "value") String email) {

		User document = userService.findByEmail(email);

		return ResponseEntity.ok().body(document);
	}

	@PostMapping
	@ApiOperation(tags = { "Usuarios" }, value = "Registra um usuário.")
	public ResponseEntity<UserDTO> save(@Valid @RequestBody UserDTO resource) throws Exception {

		// Ignore the id
		if (resource.getId() != null) {
			resource.setId(null);
		}

		User document = this.userService.saveOrUpdate(resource.toDocument());

		return new ResponseEntity<>(new UserDTO(document), HttpStatus.CREATED);
	}

	@PatchMapping("/{id}")
	@ApiOperation(tags = { "Usuarios" }, value = "Atualiza o nome do usuário por id.")
	public ResponseEntity<UserDTO> updateName(@PathVariable String id, @RequestBody UserDTO resource)
			throws Exception {

		// Id inalterável
		if (!id.equals(resource.getId())) {
			throw new WrongIdException();
		}

		User document = this.userService.findById(id);

		if (resource.getName() != null && !resource.getName().isEmpty()) {
			
			document.setName(resource.getName());
			document = this.userService.saveOrUpdate(document);
			
			return new ResponseEntity<>(new UserDTO(document), HttpStatus.OK);
		}

		return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(tags = { "Usuarios" }, value = "Exclui um usuário por id.")
	public ResponseEntity<Void> delete(@PathVariable String id) {

		User document = this.userService.findById(id);

		this.userService.delete(document.getId());

		return new ResponseEntity<>(HttpStatus.OK);
	}

}
