package br.com.unifg.api.user.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.unifg.api.exception.ObjectNotFoundException;
import br.com.unifg.api.profile.domain.Profile;
import br.com.unifg.api.profile.repository.ProfileRepository;
import br.com.unifg.api.user.domain.User;
import br.com.unifg.api.user.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private BCryptPasswordEncoder pe;

	public List<User> findAll() {
		return this.userRepository.findAll();
	}

	@Override
	public User findById(String id) {

		Optional<User> document = this.userRepository.findById(id);

		return document.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! id: " + id + ", Tipo: " + User.class.getName()));
	}

	@Override
	public User saveOrUpdate(User user) throws Exception {

		if (user.getId() == null) {

			User usuario = this.userRepository.findByEmail(user.getEmail().toLowerCase());
			if (usuario != null) {
				throw new Exception("Email já cadastrado.");
			}

			if (user.getPassword() == null || user.getPassword().isEmpty()) {
				throw new Exception("Preechimento de senha obrigatório.");
			}

			user.setPassword(pe.encode(user.getPassword()));
			
			Profile profile = this.profileRepository.save(new Profile());
			user.setProfile(profile);
		}

		return this.userRepository.save(user);
	}

	@Override
	public void delete(String id) {
		this.userRepository.deleteById(id);
	}

	@Override
	public User findByEmail(String email) {
		return this.userRepository.findByEmail(email);
	}

}
