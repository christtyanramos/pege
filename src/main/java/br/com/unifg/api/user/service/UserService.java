package br.com.unifg.api.user.service;

import java.util.List;

import br.com.unifg.api.user.domain.User;

public interface UserService {

	public List<User> findAll();

	public User findById(String id);

	public User saveOrUpdate(User user) throws Exception;

	public void delete(String id);
	
	public User findByEmail(String email);

}
