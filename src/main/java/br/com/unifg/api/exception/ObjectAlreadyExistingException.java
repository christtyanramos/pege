package br.com.unifg.api.exception;

public class ObjectAlreadyExistingException extends RuntimeException {

	private static final long serialVersionUID = 1119068719815862386L;

	public ObjectAlreadyExistingException(String msg) {
		super(msg);
	}
}
