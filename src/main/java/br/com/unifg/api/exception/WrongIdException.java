package br.com.unifg.api.exception;

import org.springframework.http.HttpStatus;

public class WrongIdException extends HttpException {

	private static final long serialVersionUID = -3243305748652578238L;

	public WrongIdException() {
		super("The id in the path is different from the object's id, in the request body", HttpStatus.BAD_REQUEST);
	}

}
