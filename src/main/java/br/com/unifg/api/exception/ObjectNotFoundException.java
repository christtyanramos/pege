package br.com.unifg.api.exception;

public class ObjectNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -954724965880507791L;

	public ObjectNotFoundException(String msg) {
		super(msg);
	}

	public ObjectNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
