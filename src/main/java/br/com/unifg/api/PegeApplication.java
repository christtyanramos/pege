package br.com.unifg.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PegeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PegeApplication.class, args);
	}

}
