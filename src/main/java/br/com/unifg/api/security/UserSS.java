package br.com.unifg.api.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Classe que atende ao contrato do Spring Security
 * 
 * @author felip
 */
public class UserSS implements UserDetails {
	private static final long serialVersionUID = 1L;

	private String id;
	private String email;
	private String password;
	private Collection<? extends GrantedAuthority> authorities;
	
	public UserSS() {}
	
	public UserSS(String id, String email, String password) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		Set<String> set = new HashSet<>();
		set.add("ROLE_CLIENTE");
		this.authorities = set.stream().map(x -> new SimpleGrantedAuthority(x)).collect(Collectors.toList());
	}

	public String getId() {
		return id;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return email;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}